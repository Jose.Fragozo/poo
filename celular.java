
import java.util.Scanner;

class celular{

public String modelo;
public String marca;
public String color;
public float tamanio;
public String so;
private String canciones[] = new String[ 10 ];
private int indice_de_canciones;

public boolean encendido;

   public celular(String modelo, String marca, String color, float tamanio, String so){
this.tamanio = tamanio;
this.so = so;
this.color = color;
this.marca = marca;
this.modelo = modelo;
this.indice_de_canciones = 0;
   }


public void encender(){
	encendido = true;
System.out.println("el telefono se ha encendido");
}

public void apagar(){
  encendido = false;
System.out.println("el telefono se ha apagado");
}

public void llamar( String numero){
	if (encendido) {
		System.out.println("el telefono esta llamdo a "+ numero);
	}else{
		System.out.println("no se puede llamar mientras esta apagado");
	}

}

public void contestar(){
if (encendido) {
  System.out.println("desea usted responder la llamada?? ");
        String entradaTeclado = "";
        Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
        entradaTeclado = entradaEscaner.nextLine ();

       if (entradaTeclado.equals("yes")) {
            System.out.println("Llamada en curso");
       }else{
           System.out.println("Llamada rechazada");
       }
}else{
  System.out.println("no se puede contestar mientras esta apagado");
}
}

public void agregarcancion(String nombre){
  if (encendido) {
    canciones[indice_de_canciones] = nombre;
      indice_de_canciones++;
      System.out.println("se agrego la cancion correctamente");
  }else{
    System.out.println("no se puede agregar canciones mientras esta apagado");
  }

}

public void escucharmusica(String cancion){
  if (encendido) {
    for (int i=0;i<indice_de_canciones;i++) {

      if(canciones[i].equals(cancion)){
           System.out.println("se esta reproduciendo la cancion"+ cancion);
      }
    }
  }else{
    System.out.println("no se puede reproducir porque esta apagado");
  }


}


}
