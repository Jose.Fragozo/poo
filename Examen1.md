## Examen unidad 1 y 2
Cree una clase llamada Empleado, que incluya tres piezas de información como 
variables de instancia: 

- nombre, 
- apellido paterno y 
- un salario mensual (double).
 
Su clase debe tener un constructor que inicialice las tres variables de 
instancia o atributos. Proporcione un método establecer y un método obtener 
para cada variable de instancia. 
Si el salario mensual no es positivo, establézcalo a 0.0. 
Escriba una aplicación de prueba llamada PruebaEmpleado o Main como mejor 
prefiera, que demuestre las capacidades de cada Empleado. 
Cree dos objetos Empleado y muestre el salario anual de cada objeto,
uno de ellos gana 8.0 al mes.

### Condiciones de entrega 
Subira un archivo .jar a su cuenta de github, el cual tendra el siguiente 
nombre: unidad1[numerodecontrol].jar, 
ejemplo: unidad112161219.jar 
ademas del codigo. 