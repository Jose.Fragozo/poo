class television{
	public String marca;
	public String modelo;
	private boolean encendido;
	public float tamanio;
	private int volumen;
	private int canal;

	public television(String marca, String modelo,float tamanio){

		this.marca = marca;
		this.modelo = modelo;
		this.tamanio = tamanio;
		encendido = false;
		volumen = 0;
		canal = 0;
	}


	public void encender(){
		if (encendido == false) {
			encendido = true;
		System.out.println("La television "+marca+ "esta encendiendo");
		}else{
			System.out.println("La television "+marca+ "  ya esta encendida");
		}

	}

	public void cambiarcanal( int c){

       if(encendido==true){
            if (c>0) {
			this.canal = c;
			System.out.println("La television   "+marca+ "cambio el canal a "+ canal);
		}
       }else{
       	System.out.println("La television   "+marca+ "no se puede cambiar el canal porque esta apagada");
       }


	}
	public void subirvolumen(){

		if (encendido) {
			volumen++;
		System.out.println("La television "+marca+ "subio el volumen a "+volumen);
		}else{
			System.out.println("La television   "+marca+ "no se puede subir el volumen porque esta apagada");
		}

	}

	public void bajarvolumen(){
		if (encendido) {
			if (volumen-1 >=0) {
				volumen--;
		System.out.println("La television "+marca+ "bajo el volumen a "+volumen);
			}else{
				System.out.println("La television "+marca+ "nose puede bajar mas el volumen");
			}

		}else{
          System.out.println("La television   "+marca+ "no se puede bajar el volumen porque esta apagada");
		}


	}

     public void apagar(){
     	if (encendido) {
     		encendido = false;
		System.out.println("La television "+marca+ "se apago");
     	}else{
     		System.out.println("La television "+marca+ " ya esta apagada");
     	}

	}

}