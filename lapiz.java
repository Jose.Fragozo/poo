class lapiz{

	public String color;
	public float  numero_de_punta;
	public String marca;
	public String tipo;

    public lapiz(float np,String color,String marca, String tipo ){
       numero_de_punta = np;
       this.color = color;
       this. marca = marca;
       this.tipo = tipo;
    }

    public void escribir (String mensaje){
        System.out.println("el lapiz marca "+ marca+"  esta escribiendo "+ mensaje);
    }
    public void dibujar (String nombre){
        System.out.println("el lapiz marca  "+marca+" esta dibujando "+ nombre);
    }
}